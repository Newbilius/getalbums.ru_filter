<?php

include_once("phpQuery-onefile.php");
header('Content-Type: text/html; charset=utf-8');
set_time_limit(0);

function print_pr($data) {
    echo "<pre>" . print_r($data, true) . "</pre>";
}

class ALB_API {

    protected $html;
    protected $last_url = "";
    public $convert = true;

    public function __construct() {
        
    }

    /**
     * Выбрать страницу для обработки. Если страница уже выбрана та же - повторно загружаться не будет.
     *
     * @param type $url ссылка на страницу
     */
    public function change_page($url) {
        if ($this->last_url != $url) {
            $this->last_url = $url;
            $this->reload();
        }
    }

    /**
     * Перезагружает текущую страницу.
     */
    public function reload() {
        $data = file_get_contents($this->last_url);
        if ($this->convert) {
            $data = iconv("windows-1251", "utf-8", $data);
            $data = str_replace("charset=windows-1251", "charset=utf-8", $data);
        };
        $this->html = phpQuery::newDocumentHTML($data, "utf-8");
        $this->html->find("div.buttons")->remove();
    }

    public function get_items() {
        $data = array();
        $rus_array = array("russian", "russia", "россия", "русск", "росси");
        foreach ($this->html->find("div.table_news") as $element) {
            $text = pq($element)->html();
            $text = str_replace('src="/uploads', ' target=_new src="http://getalbums.ru/uploads', $text);
            $small_text = mb_strtolower($text, 'UTF-8');
            $ok = false;
            foreach ($rus_array as $rus_item) {
                if (strpos($small_text, $rus_item) !== FALSE)
                    $ok = true;
            };

            if ($ok)
                $data[] = $text;
            //$data[]=$small_text;
        };
        return $data;
    }

}
?>